    Info<< "Reading field porosity\n" << endl;
    volScalarField porosity
    (
        IOobject
        (
            "porosity",
            runTime.timeName(),
            mesh,
            IOobject::MUST_READ,
            IOobject::AUTO_WRITE
        ),
        mesh
    );
    porosity.storeOldTime();

    Info<< "Reading field Dp\n" << endl;
    volScalarField Dp
    (
        IOobject
        (
            "Dp",
            runTime.timeName(),
            mesh,
            IOobject::MUST_READ,
            IOobject::AUTO_WRITE
        ),
        mesh
    );

